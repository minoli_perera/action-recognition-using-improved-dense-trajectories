% Perform Fisher Vector encoding
function code=fisher_encode_vgg(feats,pca_coeff, codebook, params, weights)
% L1 normalization & Sqare root
%feats=sqrt(feats/norm(feats,1));
feats=pca_coeff.coeff*(feats'-repmat(pca_coeff.feat_mean,size(feats,2),1))'; % Apply PCA


code = vl_fisher(single(feats), codebook.mean, codebook.variance, codebook.coef,'Fast','SquareRoot','Normalized');


end
