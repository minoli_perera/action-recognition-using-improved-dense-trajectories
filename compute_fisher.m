function [ fvt ] = compute_fisher( params, pca_coeff, gmm, file_list,t_type )
%COMPUTE_FISHER Computer postitive/negative Fisher Vectors for each class


fvt=zeros(141824,length(file_list));


fisher_params.alpha = single(0.5);  % power notmalization, 1 to disable
fisher_params.grad_weights = false; % soft BOW
fisher_params.grad_means = true;    % 1st order
fisher_params.grad_variances = true;    % 2nd order

if strcmpi(t_type,'train')==1,
	act_dir=params.dtf_dir;
	CNN_list=params.CNN_dir;
else
	act_dir=params.dtf_dir_test;
	CNN_list=params.CNN_dir_test;
end
% Make sure all files are compressed
%check_gzip_cmd=sprintf('sh ./gzip_dtf_files -i %s > /dev/null 2>&1',params.dtf_dir);
%system(check_gzip_cmd);

%feat_idx=find(strcmp(params.feat_list,feat_type)); % find the index of feature in feat_list
CNNlist=dir(fullfile(CNN_list,'*.mat'));

parfor_progress(length(file_list)); % Initialize 
parfor j=1:length(file_list)
	action=regexprep(file_list{j},'/v_(\w*)\.avi','');
	
	%act_dir=fullfile(params.dtf_dir,action);
	parfor_progress; % Count 
	clip_name=regexprep(file_list{j},'\.avi$',''); % get video clip name
	clip_name=regexprep(clip_name,'.*/','');
	file=fullfile(act_dir,[clip_name,'.txt']);
	
	[TRA,HOG,HOF,MBHx,MBHy]=extract_dtf_feats(file, params, -1);
	[CNN]=extract_CNN_fea(fullfile(CNN_list,CNNlist(j).name),params,-1);

	fv_tra=fisher_encode_vgg(TRA,pca_coeff{1},gmm{1},fisher_params);
	fv_hog=fisher_encode_vgg(HOG,pca_coeff{2},gmm{2},fisher_params);
	fv_hof=fisher_encode_vgg(HOF,pca_coeff{3},gmm{3},fisher_params);
	fv_MBHx=fisher_encode_vgg(MBHx,pca_coeff{4},gmm{4},fisher_params);
	fv_MBHy=fisher_encode_vgg(MBHy,pca_coeff{5},gmm{5},fisher_params);
	fv_CNN=fisher_encode_vgg(CNN,pca_coeff{6},gmm{6},fisher_params);

	fv=[fv_tra;fv_hog;fv_hof;fv_MBHx;fv_MBHy;fv_CNN];
	fvt(:,j)=fv;
end

% power normalization @Done in vlfeat fisher code -Ankit
%fvt = sign(fvt) .* sqrt(abs(fvt));
% L2 normalization
%fvt = double(yael_fvecs_normalize(single(fvt)));

fvt(find(isnan(fvt))) = 123456;

end

