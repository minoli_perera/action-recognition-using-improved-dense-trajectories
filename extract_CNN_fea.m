function [CNN ] = extract_dtf_feats( CNN_file, params, num_feats )
%EXTRACT_DTF_FEATS extract DTF features.
%   The first 10 elements for each line in dtf_file are information about the trajectory.
%	
%   Subsampling:
%       randomly choose 1000 descriptors from each video clip(dtf file)
%		To use all the DTF fatures, set num_feats to a negative number.
%
CNN=zeros(params.feat_len_map('CNN'),1);

if ~exist(CNN_file,'file')
	warning('File %s does not exist! Skip now...',dtf_file);
	return;
else
	tmpfile=dir(CNN_file);
	if tmpfile.bytes < 1024
		warning('File %s is too small! Skip now...',dtf_file);
		return;
	end
end


try
	x=load(CNN_file);
	x=x.code;
	if num_feats<0 % To use all the DTF fatures, set num_feats to a negative number
		num_feats=size(x,1);
	end
	
	if size(x,1)<=num_feats
		idx=1:size(x,1); % do not randomly subsample
	else
		%idx=randperm(size(x,1),num_feats); % randomly subsampling %DOES NOT WORK IN MATLAB 2010
		idx=randperm(size(x,1));
		idx=idx(1:1000);
		%idx=floor(linspace(1,size(x,1),num_feats)); % linearly subsampling
	end

	% Update randomly sampled features
	CNN=x(idx,2:end)';
catch
	warning('File %s Cannot be opened has some error..',dtf_file);
	return

end


