
%add path to VLFEAT
run('/home/asarya/software/VLFEATROOT/toolbox/vl_setup')

path=pwd;
addpath(path);
%add path to LibSVM
addpath(fullfile(path,'parfor_progress/'))
addpath(genpath('/home/asarya/software/libsvm-3.17'));
addpath(fullfile(path,'vgg_fisher'));
addpath(fullfile(path,'vgg_fisher/lib/gmm-fisher/matlab'));

%% Set Parameters
params.K=256;   % num of GMMs
params.DTF_subsample_num=1000; % Subsampling number of DTF features per video clip

params.encoding='fisher'; % encoding type: 'fisher' - fisher vector; 'bow' - bag-of-words
params.feat_list={'HOG','HOF','MBHx','MBHy'}; % all features involved in this test
feat_len={96,108,96,96}; % length of features
params.feat_len_map=containers.Map(params.feat_list, feat_len);
params.feat_start=41; % start position of DTF features

%params.dtf_dir=fullfile(path,'hollywood_dtf');
params.dtf_dir='/nfs/bigeye/sdaptardar/Datasets/Hollywood2/HollyWood2_BOF_Results/train/';
params.dtf_dir_test='/nfs/bigeye/sdaptardar/Datasets/Hollywood2/HollyWood2_BOF_Results/test/';

params.train_list_dir=fullfile(path, 'hollywoodTrainTestlist'); 
params.test_list_dir=fullfile(path, 'holywoodTrainTestlist'); 


params.train_data_info=fullfile(path,'data','UCF101_traindata_info.mat');
params.test_data_info=fullfile(path,'data','UCF101_testdata_info.mat');

% Files to store subsampled features
params.train_sample_data=fullfile(path,'data',sprintf('hollywood_train_data_sample%d_gmm%d.mat',params.DTF_subsample_num,params.K));
params.test_sample_data=fullfile(path,'data',sprintf('hollywood_test_data_sample%d_gmm%d.mat',params.DTF_subsample_num,params.K));

% Files to store Fisher vectors
params.fv_train_file=fullfile(path,'data',sprintf('pca_gmm_data_train_sample%d_gmm%d.mat',params.DTF_subsample_num,params.K));
params.fv_test_file=fullfile(path,'data',sprintf('pca_gmm_data_test_sample%d_gmm%d.mat',params.DTF_subsample_num,params.K));

matlabpool open



switch params.encoding
      case 'fisher'        
        %% Construct Fisher Vectors for training
        % Subsample DTF features, calculate PCA coefficients and train GMM model
        if ~exist(params.fv_train_file,'file') && ~exist(params.fv_test_file,'file')
            % Train and test share the same codebook?
            [ pca_coeff, gmm, all_train_files, all_train_labels, all_test_files, all_test_labels ] = pretrain(params);
            save(params.fv_train_file,'pca_coeff','gmm','all_train_files','all_train_labels','all_test_files', 'all_test_labels', '-v7.3');
        else
			sprintf('Loading DTF features ...\n')
            load(params.fv_train_file); 
        end
        
		 % Load trainning videos, computer Fisher vectors and train SVM model
        svm_option='-t 0 -s 0 -q -w0 0.5 -w1 0.5 -c 1 -b 1'; % temporarily not to use linear SVM
        uniq_labels=unique(all_train_labels);
        pred=zeros(length(all_test_files),1);
        acc=zeros(numel(uniq_labels),1);

		        
        fprintf('Staring Classification process...\n')
        for i=1:numel(uniq_labels)
             % Process training files
            pos_idx=(all_train_labels == i);
            pos_files=all_train_files(pos_idx); % positive training files
			
            fvt_pos_train=compute_fisher(params, pca_coeff, gmm, pos_files,'train');
           
            neg_idx=(all_train_labels ~= i);
            neg_files=all_train_files(neg_idx);
            %sample_idx=randperm(length(neg_files),length(pos_files));
	    idx=randperm(length(neg_files));
	    sample_idx=idx(1:length(pos_files));
   			%http://www.robots.ox.ac.uk/~vgg/software/enceval_toolkit/.    
	    sample_neg_files=neg_files(sample_idx);
            fvt_neg_train=compute_fisher(params, pca_coeff, gmm, sample_neg_files,'train');
            
            [row,col_pos]=find(fvt_pos_train==1234);
	    [row,col_neg]=find(fvt_neg_train==1234);
			
	    % Remove columns with nan values
	    fvt_pos_train(:,col_pos)=[] ;
	    fvt_neg_train(:,col_neg)=[] ;
			
            %Train SVM model
            fprintf('Training SVM %d ...\n',i)
            tmp_train_labels=[ones(1,size(fvt_pos_train,2)) -1*ones(1,size(fvt_neg_train,2))];
            model=svmtrain(double(tmp_train_labels)', double([fvt_pos_train fvt_neg_train])', svm_option);
            
            % Process test files
            pos_idx=(all_test_labels == i);
            pos_files=all_test_files(pos_idx); % positive training files
            fvt_pos_test=compute_fisher(params, pca_coeff, gmm, pos_files,'test');
            
            neg_idx=(all_test_labels ~= i);
            neg_files=all_test_files(neg_idx);
            %sample_idx=randperm(length(neg_files),length(pos_files));
	    idx=randperm(length(neg_files));
	    sample_idx=idx(1:length(pos_files));
            sample_neg_files=neg_files(sample_idx);
            fvt_neg_test=compute_fisher(params, pca_coeff, gmm, sample_neg_files,'test');
            

	   % Remove columns with nan values
	   [row,col_pos]=find(fvt_pos_test==1234);
	   [row,col_neg]=find(fvt_neg_test==1234);
	   fvt_pos_test(:,col_pos)=[] ;
	   fvt_neg_test(:,col_neg)=[] ;

            % SVM prediction
	    fprintf('Testing the model %d ...\n',i)
            tmp_test_labels=[ones(1,size(fvt_pos_test,2)) -1*ones(1,size(fvt_neg_test,2))];
            [pred_labels,accuracy,prob_estimates] = svmpredict(double(tmp_test_labels)', double([fvt_pos_test fvt_neg_test])', model, '-b 1');
            accuracy= sum(pred_labels(1:size(fvt_pos_test,2)) == 1) ./ size(fvt_pos_test,2);    %# accuracy
			acc(i)=accuracy;
            pred(pos_idx) = (pred_labels(1:size(fvt_pos_test,2))==1)*i;
            %pred = [pred; (pred_labels(1:size(fvt_pos_test,2))==1)*i];
            
	    save_file=sprintf('fisher_vectors_action%d_sample%d_gmm%d.mat',i,params.DTF_subsample_num,params.K);
	    save(fullfile(path,'data',save_file),'fvt_pos_train','fvt_neg_train', 'fvt_pos_test', 'fvt_neg_test','tmp_train_labels','pred_labels','tmp_test_labels', '-v7.3');
        end

        %fprintf('Mean accuracy: %f.\n', mean(acc)); % display mean accuracy 
		fprintf('\nMean accuracy: %f.\n', sum(pred==all_test_labels)/numel(all_test_labels)); % display mean accuracy 
        save(pred_results,'pred','acc','-v7.3');
      
end

matlabpool close
